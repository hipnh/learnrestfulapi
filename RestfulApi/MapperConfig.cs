﻿using AutoMapper;
using Model;
using Model.DTO.ClassDTO;
using Model.DTO.StudentDTO;
using Model.DTO.UserDTO;

namespace RestfulApi
{
    public class MapperConfig:Profile
    {
        public MapperConfig()
        {
            CreateMap<Student, StudentDTO>().ReverseMap();
            CreateMap<StudentUpdateDTO, Student>().ReverseMap();
            CreateMap<StudentCreateDTO, Student>().ReverseMap();

            CreateMap<Class, ClassDTO>().ReverseMap();
            CreateMap<ClassUpdateDTO, Class>().ReverseMap();
            CreateMap<ClassCreateDTO, Class>().ReverseMap();

            CreateMap<LocalUser, UserDTO>().ReverseMap();
        }
    }
}
