﻿using AutoMapper;
using DataAccess.IRepository;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Mvc;
using Model;
using Model.DTO.StudentDTO;
using System.Net;

namespace RestfulApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class StudentController : ControllerBase
    {
        protected APIResponse _response;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public StudentController(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
            _response = new APIResponse();
        }

        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<IActionResult> GetStudent()
        {
            IEnumerable<Student> student = await _unitOfWork.Students.GetAllAsync();
            _response.Result = _mapper.Map<List<StudentDTO>>(student);
            _response.StatusCode = HttpStatusCode.OK;
            return Ok(_response);
        }

        [HttpGet("{id:int}", Name = "GetByIdStudent")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> GetById(int id)
        {
            if (id == 0)
            {
                return BadRequest("id not found");
            }
            var student = await _unitOfWork.Students.GetAsync(x => x.Id == id);
            if (student == null)
                return NotFound();

            _response.Result = _mapper.Map<StudentDTO>(student);
            _response.StatusCode = HttpStatusCode.OK;
            return Ok(_response);
        }

        [HttpPost(Name = "CreateStudent")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> Create([FromBody] StudentCreateDTO studentDTO)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            if (studentDTO == null)
                return BadRequest(studentDTO);

            Student student = _mapper.Map<Student>(studentDTO);

            await _unitOfWork.Students.AddAsync(student);
            await _unitOfWork.SaveAsync();
            _response.Result = _mapper.Map<StudentCreateDTO>(student);
            _response.StatusCode = HttpStatusCode.Created;
            return CreatedAtRoute("GetByIdStudent", new{id = student.Id}, _response);
        }

        [HttpDelete("{id:int}", Name = "DeleteStudent")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> Delete(int id)
        {
            if (id == 0)
                return BadRequest();
            var student = await _unitOfWork.Students.GetAsync(x => x.Id == id);
            if (student == null)
                return NotFound();
            await _unitOfWork.Students.RemoveAsync(student);
            await _unitOfWork.SaveAsync();

            return NoContent();
        }

        [HttpPut("{id:int}",Name = "UpdateStudent")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> Update(int id, [FromBody] StudentUpdateDTO newStudent)
        {
            if (id != newStudent.Id || newStudent == null)
                return BadRequest();

            var student = await _unitOfWork.Students.GetAsync(x => x.Id == id, false);
            if (student == null)
                return NotFound();

            student = _mapper.Map<Student>(newStudent);
            await _unitOfWork.Students.UpdateAsync(student);
            await _unitOfWork.SaveAsync();

            return NoContent();
        }

        [HttpPatch("{id:int}",Name = "UpdatePartialStudent")]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        public async Task<IActionResult> UpdatePartial(int id, JsonPatchDocument<StudentUpdateDTO> patchDTO)
        {
            if (id == 0 || patchDTO == null)
                return BadRequest();

            var student = await _unitOfWork.Students.GetAsync(x => x.Id == id, false);
            if (student == null)
                return NotFound();

            var studentDTO = _mapper.Map<StudentUpdateDTO>(student);
            patchDTO.ApplyTo(studentDTO, ModelState);
            student = _mapper.Map<Student>(studentDTO);

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            await _unitOfWork.Students.UpdateAsync(student);
            await _unitOfWork.SaveAsync();

            return NoContent();
        }
    }
}
