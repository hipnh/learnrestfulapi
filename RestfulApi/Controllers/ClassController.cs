﻿using AutoMapper;
using DataAccess.IRepository;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Mvc;
using Model;
using Model.DTO.ClassDTO;
using System.Net;

namespace RestfulApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ClassController : ControllerBase
    {
        protected APIResponse _response;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public ClassController(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
            _response = new APIResponse();
        }

        [HttpGet]
       //[Authorize]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        public async Task<IActionResult> GetClass()
        {
            IEnumerable<Class> student = await _unitOfWork.Classes.GetAllAsync();
            _response.Result = _mapper.Map<List<ClassDTO>>(student);
            _response.StatusCode = HttpStatusCode.OK;
            return Ok(_response);
        }

        [HttpGet("{id:int}", Name = "GetByIdClass")]
        //[Authorize]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> GetById(int id)
        {
            if (id == 0)
            {
                return BadRequest("id not found");
            }
            var student = await _unitOfWork.Classes.GetAsync(x => x.Id == id);
            if (student == null)
                return NotFound();

            _response.Result = _mapper.Map<ClassDTO>(student);
            _response.StatusCode = HttpStatusCode.OK;
            return Ok(_response);
        }

        [HttpPost(Name = "CreateClass")]
        //[Authorize(Roles ="admin")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> Create([FromBody] ClassCreateDTO studentDTO)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            if (studentDTO == null)
                return BadRequest(studentDTO);

            Class student = _mapper.Map<Class>(studentDTO);

            await _unitOfWork.Classes.AddAsync(student);
            await _unitOfWork.SaveAsync();

            _response.Result = _mapper.Map<ClassCreateDTO>(student);
            _response.StatusCode = HttpStatusCode.Created;
            return CreatedAtRoute("GetByIdClass", new { id = student.Id }, _response);
        }

        [HttpDelete("{id:int}", Name = "DeleteClass")]
        //[Authorize(Roles = "admin")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> Delete(int id)
        {
            if (id == 0)
                return BadRequest();
            var student = await _unitOfWork.Classes.GetAsync(x => x.Id == id);
            if (student == null)
                return NotFound();
            await _unitOfWork.Classes.RemoveAsync(student);
            await _unitOfWork.SaveAsync();

            return NoContent();
        }

        [HttpPut("{id:int}", Name = "UpdateClass")]
        //[Authorize(Roles = "admin")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<IActionResult> Update(int id, [FromBody] ClassUpdateDTO newClass)
        {
            if (id != newClass.Id || newClass == null)
                return BadRequest();

            var student = await _unitOfWork.Classes.GetAsync(x => x.Id == id, false);
            if (student == null)
                return NotFound();

            student = _mapper.Map<Class>(newClass);
            await _unitOfWork.Classes.UpdateAsync(student);
            await _unitOfWork.SaveAsync();

            return NoContent();
        }

        [HttpPatch("{id:int}", Name = "UpdatePartialClass")]
        //[Authorize(Roles = "admin")]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        public async Task<IActionResult> UpdatePartial(int id, JsonPatchDocument<ClassUpdateDTO> patchDTO)
        {
            if (id == 0 || patchDTO == null)
                return BadRequest();

            var Class = await _unitOfWork.Classes.GetAsync(x => x.Id == id, false);
            if (Class == null)
                return NotFound();

            var classDTO = _mapper.Map<ClassUpdateDTO>(Class);
            patchDTO.ApplyTo(classDTO, ModelState);
            Class = _mapper.Map<Class>(classDTO);

            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            } 
            await _unitOfWork.Classes.UpdateAsync(Class);
            await _unitOfWork.SaveAsync();

            return NoContent();
        }
    }
}