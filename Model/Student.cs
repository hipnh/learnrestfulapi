﻿using System.ComponentModel.DataAnnotations;

namespace Model
{
    public class Student
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public int ClassId { get; set; }
        public virtual Class Class { get; set; }

        public virtual ICollection<Score> Scores { get; set; }
    }
}
