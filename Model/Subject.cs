﻿namespace Model
{
    public class Subject
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }

        public virtual ICollection<Score> Scores { get; set; }
    }
}
