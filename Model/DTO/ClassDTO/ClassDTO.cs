﻿namespace Model.DTO.ClassDTO
{
    public class ClassDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
    }
}
