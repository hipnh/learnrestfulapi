﻿namespace Model.DTO.ClassDTO
{
    public class ClassCreateDTO
    {
        public string Name { get; set; }
        public string Description { get; set; }
    }
}
