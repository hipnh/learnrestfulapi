﻿using System.ComponentModel.DataAnnotations;

namespace Model.DTO.StudentDTO
{
    public class StudentDTO
    {
        [Key]
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }
        public string Description { get; set; }
    }
}
