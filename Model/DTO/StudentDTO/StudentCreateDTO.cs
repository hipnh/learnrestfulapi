﻿namespace Model.DTO.StudentDTO
{
    public class StudentCreateDTO
    {
        public string Name { get; set; }
        public string Description { get; set; }
    }
}
