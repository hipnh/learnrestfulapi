﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Model;
using Newtonsoft.Json;
using StudentManagement_Web.Models.DTO.ClassDTO;
using StudentManagement_Web.Services.IServices;

namespace StudentManagement_Web.Controllers
{
    public class ClassController : Controller
    {
        private readonly IClassService _classService;
        private readonly IMapper _mapper;

        public ClassController(IClassService classService, IMapper mapper)
        {
            _classService = classService;
            _mapper = mapper;
        }
        public async Task<IActionResult> Index()
        {
            List<ClassDTO> classes = new();
            var response = await _classService.GetAllAsync<APIResponse>();
            if (response != null && response.IsSuccess)
            {
                classes = JsonConvert.DeserializeObject<List<ClassDTO>>(Convert.ToString(response.Result));
            }
            return View(classes);
        }
        public async Task<IActionResult> Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(ClassCreateDTO dto)
        {
            if (ModelState.IsValid)
            {
                var response = await _classService.CreateAsync<APIResponse>(dto);
                if (response != null && response.IsSuccess)
                {
                    return RedirectToAction("Index");
                }
                return View(dto);
            }

            return View();
        }

        public async Task<IActionResult> Edit(int ClassId)
        {
            var response = await _classService.GetAsync<APIResponse>(ClassId);
            if (response != null && response.IsSuccess)
            {
                return View(response);
            }
            return RedirectToAction("Index");
        }

        [HttpPost]
        public async Task<IActionResult> Edit(ClassUpdateDTO dto)
        {
            await Console.Out.WriteLineAsync("EditPostEditPostEditPostEditPost");
            return View(dto);
        }

    }
}
