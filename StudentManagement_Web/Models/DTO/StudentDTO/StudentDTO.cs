﻿using System.ComponentModel.DataAnnotations;

namespace StudentManagement_Web.Models.DTO.ClassDTO
{
    public class StudentDTO
    {
        [Key]
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }
        public string Description { get; set; }
    }
}
