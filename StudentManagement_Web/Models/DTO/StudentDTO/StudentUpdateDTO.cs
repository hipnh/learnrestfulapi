﻿using System.ComponentModel.DataAnnotations;

namespace StudentManagement_Web.Models.DTO.ClassDTO
{
    public class StudentUpdateDTO
    {
        [Key]
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }
        public string Description { get; set; }
    }
}
