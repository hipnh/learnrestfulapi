﻿namespace StudentManagement_Web.Models.DTO.ClassDTO
{
    public class ClassCreateDTO
    {
        public string Name { get; set; }
        public string Description { get; set; }
    }
}
