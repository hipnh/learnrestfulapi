﻿namespace StudentManagement_Web.Models.DTO.ClassDTO
{
    public class ClassUpdateDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
    }
}
