﻿using StudentManagement_Utility;
using StudentManagement_Web.Models;
using StudentManagement_Web.Models.DTO.ClassDTO;
using StudentManagement_Web.Services.IServices;

namespace StudentManagement_Web.Services
{
    public class ClassService : BaseService, IClassService
    {
        private readonly IHttpClientFactory _httpClient;
        private string _classUrl;
        private string subUrl = "class";
        public ClassService(IHttpClientFactory httpClient, IConfiguration configuration) : base(httpClient)
        {
            _httpClient = httpClient;
            _classUrl = configuration.GetValue<string>("ServiceUrls:StudentManagementAPI").ToString();
        }

        public async Task<T> CreateAsync<T>(ClassCreateDTO dto)
        {
            return await SendAsync<T>(new APIRequest()
            {
                ApiType = SD.ApiType.POST,
                Data = dto,
                Url = _classUrl + "/api/" + subUrl
            });
        }

        public Task<T> DeleteAsync<T>(int id)
        {
            return SendAsync<T>(new APIRequest()
            {
                ApiType = SD.ApiType.DELETE,
                Url = _classUrl + "/api/" + subUrl + id
            });
        }

        public async Task<T> GetAllAsync<T>()
        {
            return await SendAsync<T>(new APIRequest()
            {
                ApiType = SD.ApiType.GET,
                Url = _classUrl + "/api/" + subUrl
            });
        }

        public async Task<T> GetAsync<T>(int id)
        {
            await Console.Out.WriteLineAsync(_classUrl + "/api/" + subUrl + "/" + id);
            return await SendAsync<T>(new APIRequest()
            {
                ApiType = SD.ApiType.GET,
                Url = _classUrl + "/api/" + subUrl + "/" + id
            });
        }

        public async Task<T> UpdateAsync<T>(ClassUpdateDTO dto)
        {
            return await SendAsync<T>(new APIRequest()
            {
                ApiType = SD.ApiType.PUT,
                Data = dto,
                Url = _classUrl + "/api/" + subUrl + dto.Id
            });
        }
    }
}
