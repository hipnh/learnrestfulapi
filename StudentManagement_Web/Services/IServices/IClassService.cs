﻿using StudentManagement_Web.Models.DTO.ClassDTO;

namespace StudentManagement_Web.Services.IServices
{
    public interface IClassService
    {
        Task<T> GetAllAsync<T>();
        Task<T> GetAsync<T>(int id);
        Task<T> CreateAsync<T>(ClassCreateDTO dto);
        Task<T> UpdateAsync<T>(ClassUpdateDTO dto);
        Task<T> DeleteAsync<T>(int id);
    }
}
