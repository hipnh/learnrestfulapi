﻿using AutoMapper;
using StudentManagement_Web.Models.DTO.ClassDTO;

namespace StudentManagement_Web
{
    public class MapperConfig:Profile
    {
        public MapperConfig()
        {
            CreateMap<ClassDTO, ClassCreateDTO>().ReverseMap();
            CreateMap<ClassDTO, ClassUpdateDTO>().ReverseMap();
        }
    }
}
