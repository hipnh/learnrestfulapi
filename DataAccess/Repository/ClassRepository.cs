﻿using DataAccess.IRepository;
using Microsoft.EntityFrameworkCore;
using Model;

namespace DataAccess.Repository
{
    public class ClassRepository : Repository<Class>, IClassRepository
    {
        public ClassRepository(ApplicationDbContext applicationDbContext) : base(applicationDbContext)
        {
        }
        public async Task<List<Class>> GetAllStudentByIdAsync(int id)
        {
            return await _db.Where(c => c.Id == id).Include(s=>s.Students).ToListAsync();
        }
    }
}