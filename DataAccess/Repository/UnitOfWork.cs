﻿using DataAccess.IRepository;

namespace DataAccess.Repository
{
    public class UnitOfWork : IUnitOfWork
    {
        ApplicationDbContext _db;
        public UnitOfWork(ApplicationDbContext db)
        {
            _db = db;
            Students = new StudentRepository(_db);
            Classes = new ClassRepository(_db);
        }
        public IStudentRepository Students { get; set; }
        public IClassRepository Classes { get; set; }

        public async Task SaveAsync()
        {
            await _db.SaveChangesAsync();  
        }
    }
}
