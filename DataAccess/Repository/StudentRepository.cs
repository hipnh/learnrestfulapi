﻿using DataAccess.IRepository;
using Microsoft.EntityFrameworkCore;
using Model;

namespace DataAccess.Repository
{
    public class StudentRepository : Repository<Student>, IStudentRepository
    {
        public StudentRepository(ApplicationDbContext applicationDbContext) : base(applicationDbContext)
        {
        }
    }
}
