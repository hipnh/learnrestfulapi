﻿using Model;

namespace DataAccess.IRepository
{
    public interface IClassRepository:IRepository<Class>
    {
        Task<List<Class>> GetAllStudentByIdAsync(int id);
    }
}
