﻿namespace DataAccess.IRepository
{
    public interface IUnitOfWork
    {
        IStudentRepository Students { get; set; }
        IClassRepository Classes { get; set; }
        Task SaveAsync();
    }
}
