﻿using Model.DTO.UserDTO;

namespace DataAccess.IRepository
{
    public interface IAuthService
    {
        Task<T> LoginAsync<T>(LoginRequestDTO objToCreate);
        Task<T> RegisterAsync<T>(UserDTO objToCreate);
    }
}
