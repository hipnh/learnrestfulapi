﻿using Model;

namespace DataAccess.IRepository
{
    public interface IStudentRepository:IRepository<Student>
    {
    }
}
