﻿using Microsoft.EntityFrameworkCore;
using Model;

namespace DataAccess
{
    public partial class ApplicationDbContext
    {
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            //Configuation
            //Model Configurations

            //Entity Configuration
            modelBuilder.Entity<Student>()
                .HasOne(c => c.Class)
                .WithMany(s => s.Students).IsRequired()
                .HasForeignKey(s => s.ClassId)
                .OnDelete(DeleteBehavior.Cascade);

            modelBuilder.Entity<Score>()
                .HasOne(c => c.Student)
                .WithMany(s => s.Scores)
                .IsRequired()
                .HasForeignKey(s => s.StudentId)
                .OnDelete(DeleteBehavior.Cascade);

            modelBuilder.Entity<Score>()
                .HasOne(c => c.Subject)
                .WithMany(s => s.Scores)
                .IsRequired()
                .HasForeignKey(s => s.SubjectId)
                .OnDelete(DeleteBehavior.Cascade);

            //Property Configuration
            //--Class
            modelBuilder.Entity<Class>().Property("Name").HasMaxLength(100).IsRequired();

            //--Student
            modelBuilder.Entity<Student>().Property("Name").HasMaxLength(100).IsRequired();
            modelBuilder.Entity<Student>().Property("Email").IsRequired();
            modelBuilder.Entity<Student>().HasIndex(s => s.Email).IsUnique();

            //--Subject
            modelBuilder.Entity<Subject>().Property("Name").HasMaxLength(100).IsRequired();

            //--Score
            modelBuilder.Entity<Score>().Property("ScoreValue").IsRequired();



            //Create seed data
            modelBuilder.Entity<Class>().HasData(
               new Class()
               {
                   Id = 1,
                   Name = "Class Test 1",
                   Description = "This is Description 1"
               },
               new Class()
               {
                   Id = 2,
                   Name = "Class Test 2",
                   Description = "This is Description 2"

               },
               new Class()
               {
                   Id = 3,
                   Name = "Class Test 3",
                   Description = "This is Description 3"

               });

            modelBuilder.Entity<Student>().HasData(
                new Student()
                {
                    Id = 1,
                    Name = "Student Test 1",
                    Email = "hiep@gmail.com",
                    ClassId = 1
                },
                new Student()
                {
                    Id = 2,
                    Name = "Student Test 2",
                    Email = "aaa@gmail.com",
                    ClassId = 2
                },
                new Student()
                {
                    Id = 3,
                    Name = "Student Test 3",
                    Email = "333s2@gmail.com",
                    ClassId = 2
                });

            modelBuilder.Entity<Subject>().HasData(
                new Subject()
                {
                    Id = 1,
                    Name = "Subject Test 1",
                    Description = "This is Description 1"
                },
                new Subject()
                {
                    Id = 2,
                    Name = "Subject Test 2",
                    Description = "This is Description 2"
                },
                new Subject()
                {
                    Id = 3,
                    Name = "Subject Test 3",
                    Description = "This is Description 3"
                });

            modelBuilder.Entity<Score>().HasData(
                 new Score()
                 {
                     Id = 1,
                     ScoreValue = 1.3f,
                     StudentId = 1,
                     SubjectId = 1
                 },
                 new Score()
                 {
                     Id = 2,
                     ScoreValue = 5.8f,
                     StudentId = 2,
                     SubjectId = 1
                 },
                 new Score()
                 {
                     Id = 3,
                     ScoreValue = 7.9f,
                     StudentId = 2,
                     SubjectId = 2
                 },
                 new Score()
                 {
                     Id = 4,
                     ScoreValue = 7.9f,
                     StudentId = 2,
                     SubjectId = 3
                 });

        }
    }
}
